# Contributing to secops.space

Before starting anything off, check in /issues to see if anything similar to
what you want to do has been requested before.  
If it has not, open a new feature request explaining in details your idea and
wait for @bbriggs or @jxhn to greenlight it.  
Alternatively, you can touch base on IRC with fraq (@bbriggs) or \_0x6e756c6c
(@jxhn) to expedite this process.

## Tech Stack

The repo is based on [next.js](https://nextjs.org/) (server rendered react).

Go to [README.md](README.md) for more info on how to set up your env.

The website is hosted with [Zeit Now](https://zeit.co/now) and every commit to
master is deployed automatically.  
Also, every PR is built and deployed to make testing and code-review easy, thus
each PR is its own staging env.

## Branching Model and GitHub Flow

> GitHub Flow is a lightweight, branch-based workflow that supports teams and
> projects where deployments are made regularly.

If you're unfamiliar with it, we recommend that you
[read about it](https://guides.github.com/introduction/flow/).

Simply put:

### Permanent Branch

We only have one permanent branch which is `master`, and expectedly is the
production branch.  
Every merge is effectively a new release, as it is automatically deployed to
prod.

### Support Branches

We have 2 types of support branches: `feature` and `hotfix` that are both
branched from `master`.

```
  source prod release         hotfix live         target prod release
                    v         v                   v
[      master ] ____o_________o_________o_________o________
                     \       /                   /
[  hotfix/bug ]       \__o__o                   /
                       \     \                 /
[ feature/new ]         \_____o_____o____o____o

```

See the actual
[network of the repo](https://github.com/secops-space/secops.space/network).

### Naming

We prefix the name of the branch with its type: i.e. `feature/newfeature` or
`hotfix/bugtofix`. That makes it really easy for anyone to understand the basic
purpose of that branch and improves house keeping.

## Committing

When writing your commit message, follow those rules:

1. Separate subject from body with a blank line
2. Limit the subject line to 50 characters
3. Capitalize the subject line
4. Do not end the subject line with a period
5. Use the imperative mood in the subject line
6. Wrap the body at 72 characters
7. Use the body to explain what and why vs. how

The body is also the appropriate place to link your commit to a github issue
(i.e. writing "Fix #6").

Read more about
[why good commit message matters](https://chris.beams.io/posts/git-commit/).

## Linting and Prettifying

Eslint is used in the repo and we encourage you to activate it with your editor.
We use eslint:recommend as a base and modify a couple of rules:

- arrow-spacing: arrow function arrows must have space before and after
- indent: 2 spaces
- jsx-quotes: components prop uses single quotes
- no-console: the use of console will trigger a warning (not an error)
- no-duplicate-imports: group your imports from the same file together
- no-useless-contructor: I mean, if it's useless, why have it
- no-var: it's ES248674 people, we use const and let now
- quotes: single quotes all the way
- sort-imports: imports should be sorted alphabetically, ignoring case
- sort-keys: object keys should be sorted, ascendingly
- sort-vars: multi var declaration should be sorted alphabetically
- space-before-blocks: put a space before functions, classes and keywords blocks
- space-before-function-paren: put a space before function parenthesis
- space-in-parens: noop, no spaces in the parenthesis
- spaced-comment: space expected afte /\* or //
- semi: no semicolons, unless for disambiguation

## Dependencies

Any addition of dependency must be thoroughly justified and evaluated. The KISS
principle is still relevant and to quote @jxhn:

> This isn't the next myspace

## Readability over fancyness / one liner / mind blowing shortcut

We all enjoy witticism as a form of prowess, but far less when you come back to
a piece of code months later and can't remember what it was for.

So please, make it enjoyable to read your code for someone that has to do a
hotfix on prod, late at night, after a long day at work, that asked for a beer
and was offered a bud light.
