import Article from '../components/post/article'
import Head from 'next/head'
import { news } from '../posts'
import Page from '../layouts/main'

export default () => (
  <Page>
    <Head>
      <title>News</title>
    </Head>
    <div className='newses'>
      {news.map(({ id, date, title }, i) => (
        <Article id={id} key={i} source='news' date={date} title={title} />
      ))}
    </div>
  </Page>
)
