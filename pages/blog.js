import Article from '../components/post/article'
import Head from 'next/head'
import Page from '../layouts/main'
import { posts } from '../posts'

export default () => (
  <Page>
    <Head>
      <title>Posts</title>
    </Head>
    <div className='blogses'>
      {posts.map(({ id, date, title }, i) => (
        <Article id={id} key={i} source='blog' date={date} title={title} />
      ))}
    </div>
  </Page>
)
