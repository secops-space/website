import Code from '../../components/post/code'
import Head from 'next/head'
import Page from '../../layouts/main'

export default () => (
  <Page>
    <Head>
      <title>CORPCTF</title>
    </Head>
    <div className='corpctf'>
      <Code># apropos CORPCTF</Code>
    </div>
  </Page>
)
