/* eslint-disable */
import List, { LI } from '../components/post/numbers-list'
import Head from 'next/head'
import Page from '../layouts/main'
import Title from '../components/post/title'
import H2 from '../components/post/heading'
import P from '../components/post/paragraph'
/* eslint-enable */

const Contender = ({ nick, logos }) => {
  return logos.map(l => (
    <LI key={nick + l}>
      <a
        href={`/static/logos/${nick}/${l}`}
        rel='noopener noreferrer'
        target='_blank'
      >
        <img src={`/static/logos/${nick}/${l}`} />
      </a>
      <style jsx>{`
        img {
          max-height: 200px;
          max-width: 400px;
        }
      `}</style>
    </LI>
  ))
}

export default () => (
  <Page>
    <Head>
      <title>Logos</title>
    </Head>
    <div className='logos'>
      <Title>Logo Contest</Title>
      <P>
        These wonderful logos were submitted by our community, each taking hours
        of blood, work and sweat; so at least spend a few minutes on each
        reflecting on its qualities.
      </P>
      <section>
        <H2>_0x6e756c6c</H2>
        <List className='contenders'>
          <Contender
            nick='_0x6e756c6c'
            logos={['logo1.svg', 'logo2.svg', 'logo3.svg', 'logo4.svg']}
          />
        </List>
      </section>
      <section>
        <H2>Abyss</H2>
        <List className='contenders'>
          <Contender nick='Abyss' logos={['logo1.png']} />
        </List>
      </section>
      <section>
        <H2>binia</H2>
        <List className='contenders'>
          <Contender nick='binia' logos={['logo1.svg']} />
        </List>
      </section>
      <section>
        <H2>CriticalCow</H2>
        <List className='contenders'>
          <Contender nick='CriticalCow' logos={['logo1.jpg']} />
        </List>
      </section>
      <section>
        <H2>deshi</H2>
        <List className='contenders'>
          <Contender nick='deshi' logos={['logo1.png']} />
        </List>
      </section>
      <section>
        <H2>Disdain</H2>
        <List className='contenders'>
          <Contender
            nick='Disdain'
            logos={[
              'logo1.jpg',
              'logo2.jpg',
              'logo3.jpg',
              'logo4.jpg',
              'logo5.jpg',
            ]}
          />
        </List>
      </section>
      <section>
        <H2>egy</H2>
        <List className='contenders'>
          <Contender nick='egy' logos={['logo1.jpg']} />
        </List>
      </section>
      <section>
        <H2>leeky</H2>
        <List className='contenders'>
          <Contender nick='leeky' logos={['logo1.png']} />
        </List>
      </section>
      <section>
        <H2>nugget</H2>
        <List className='contenders'>
          <Contender nick='nugget' logos={['logo1.png', 'logo2.png']} />
        </List>
      </section>
      <section>
        <H2>Pyhscript</H2>
        <List className='contenders'>
          <Contender nick='Pyhscript' logos={['logo1.png']} />
        </List>
      </section>
      <section>
        <H2>rain</H2>
        <List className='contenders'>
          <Contender nick='rain' logos={['logo1.png']} />
        </List>
      </section>
      <section>
        <H2>REal0day</H2>
        <List className='contenders'>
          <Contender nick='REal0day' logos={['logo1.png']} />
        </List>
      </section>
      <section>
        <H2>Shazzberry</H2>
        <List className='contenders'>
          <Contender nick='Shazzberry' logos={['logo1.png']} />
        </List>
      </section>
      <section>
        <H2>skiddo</H2>
        <List className='contenders'>
          <Contender nick='skiddo' logos={['logo1.png']} />
        </List>
      </section>
      <section>
        <H2>Viking</H2>
        <List className='contenders'>
          <Contender
            nick='Viking'
            logos={['logo1.png', 'logo2.png', 'logo3.png']}
          />
        </List>
      </section>
      <section>
        <H2>void</H2>
        <List className='contenders'>
          <Contender nick='void' logos={['logo1.svg']} />
        </List>
      </section>
    </div>
  </Page>
)
